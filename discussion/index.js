// setup dependencies
const express = require("express");
const mongoose = require("mongoose");
require("dotenv").config();

//call routes
const taskRoute = require("./routes/taskRoute");
//server setup
const app = express();
const port = 3001;
app.use(express.json());
app.use(express.urlencoded({extended:true}));

//database connection
mongoose.connect(
  `mongodb+srv://kendrick123:${process.env.MONGODB_PASSWORD}@cluster0.nkwttx3.mongodb.net/MRC?retryWrites=true&w=majority`,
  { useNewUrlParser: true, useUnifiedTopology: true }
);

let db = mongoose.connection;
//error handling
db.on("error", console.error.bind(console, "Conn error"));
db.on("open", () => console.log("Connected to database"));

//call routes
app.use("/tasks", taskRoute);


app.listen(port, ()=> console.log(`Server listening at port: ${port}`));