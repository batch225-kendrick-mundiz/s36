
const express = require("express");

const router = express.Router();

const taskController = require("../controllers/taskController");

router.get("/:id", (req, res) => {
	taskController.getSpecificTask(req.params.id).then(result => res.send(result));
})

router.put("/updateStatus/:id/:status", (req, res) =>{
    taskController.updateStatus(req.params.id, req.params.status)
    .then(result => res.send(result));
});
module.exports = router;