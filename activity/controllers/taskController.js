const Task = require("../models/task")

module.exports.getSpecificTask = (taskId) => {
	return Task.findById(taskId).then(result => {
		return result;
	})
}

module.exports.updateStatus = (taskId, newStatus) => {
    return Task.findByIdAndUpdate(taskId, {status : newStatus})
    .then((result, err) => {
        if(err){
            console.log(err);
            return false;
        }else{
            return Task.findById(taskId).then(result => {
			return result;
		})
        }
    });
}

